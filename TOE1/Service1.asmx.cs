﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls.WebParts;

namespace TOE1
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod(Description = "Metodo que faz Soma!")]
        public decimal Calcaula(decimal Valor1, decimal Valor2)
        {
            return Valor1 + Valor2;
        }

        [WebMethod(Description = "Metodo que faz a Subtração")]
        public decimal Subtracao(decimal Valor1,decimal Valor2)
        {
            return Valor1 - Valor2;
        }

        [WebMethod(Description = "Metodo que faz Multiplicação")]
        public decimal Multiplicacao(decimal Valor1,decimal Valor2)
        {
            return Valor1*Valor2;
        }

        [WebMethod(Description = "Metodo que faz divisão")]
        public decimal Divisao(decimal Valor1, decimal Valor2)
        {
            return (Valor1/Valor2);
        }
    }
}